package com.rcontarini.webmotors.ui.home

import com.rcontarini.webmotors.data.network.model.VehicleResponse
import com.rcontarini.webmotors.ui.BaseContract

interface HomeContract {

    interface View : BaseContract.View {
        fun displayError(msg: String?)
        fun displayVehicles(listVehicle: List<VehicleResponse>)
        fun loadVehicleDetail(item: VehicleResponse)
        fun showLoading(loading : Boolean)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun getVehicles(page : Int)
        fun onClickItem(item : VehicleResponse)
        fun setVehicles(listVehicle : List<VehicleResponse>)
        fun setError(error : Throwable)
    }

    interface Model : BaseContract.Model<Presenter>{
        fun loadMovies(page : Int)
    }

}