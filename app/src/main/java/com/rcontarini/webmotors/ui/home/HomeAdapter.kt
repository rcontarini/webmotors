package com.rcontarini.webmotors.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rcontarini.webmotors.R
import com.rcontarini.webmotors.data.network.model.VehicleResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_vehicle.view.*

class HomeAdapter(private val context: Context, private val onItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mList: List<VehicleResponse> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_vehicle, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is ItemViewHolder){
            holder.bind(mList[position], onItemClickListener)
        }
    }

    fun setVehicles(listVehicle: List<VehicleResponse>){
        mList = listVehicle
    }

    interface OnItemClickListener{
        fun onItemClicked( item : VehicleResponse )
    }

    inner class ItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun bind(item : VehicleResponse, onItemClickListerner: OnItemClickListener){

            itemView.apply {
                setOnClickListener {
                    onItemClickListerner.onItemClicked( item )
                }

                Picasso.get().load(item.Image).into(vehiclePoster)
            }
        }
    }
}