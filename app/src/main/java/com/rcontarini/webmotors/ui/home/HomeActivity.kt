package com.rcontarini.webmotors.ui.home

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rcontarini.webmotors.R
import com.rcontarini.webmotors.data.network.model.VehicleResponse
import com.rcontarini.webmotors.ui.BaseActivity
import com.rcontarini.webmotors.ui.genericDialog.GenericDialog
import com.rcontarini.webmotors.ui.genericDialog.GenericDialogImageType
import com.rcontarini.webmotors.utils.EndlessRecyclerViewScrollListener
import com.rcontarini.webmotors.utils.extensions.setVisible
import com.rcontarini.webmotors.utils.extensions.setup
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity(), HomeContract.View {

    lateinit var layoutManager: LinearLayoutManager
    private lateinit var mAdapter: HomeAdapter

    private val mPresenter: HomeContract.Presenter by lazy {
        val presenter = HomePresenter()
        presenter.attach(this)
        presenter
    }

    private val mEndlessScrollListener: EndlessRecyclerViewScrollListener by lazy {
        object : EndlessRecyclerViewScrollListener(rvHome.layoutManager as LinearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                mPresenter.getVehicles(page)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setup()
        setListeners()
        setViews()
        mPresenter.getVehicles(1)
    }

    private fun setViews(){
        mAdapter = HomeAdapter(this, object : HomeAdapter.OnItemClickListener {
            override fun onItemClicked(item: VehicleResponse) {
                mPresenter.onClickItem(item)
            }
        })
        layoutManager = LinearLayoutManager(context)
        rvHome.addOnScrollListener(mEndlessScrollListener)
        rvHome.setup(mAdapter, layoutManager)
    }

    private fun setListeners() {
    }

    private fun setup() {
        setToolbar("")

    }

    override fun displayError(msg: String?) {
        GenericDialog(context,
            imageType = GenericDialogImageType.FACE_SAD,
            title = getString(R.string.dialog_error),
            htmlSubtitle = msg,
            negativeButtonTitle = null,
            positiveButtonTitle = getString(R.string.positive_button),
            onDialogClickListener = object : GenericDialog.OnDialogClickListener {

                override fun onDialogPositiveClicked() {
                }

                override fun onDialogNegativeClicked() {
                }

            }, isCancelable = false
        ).show()
    }

    override fun displayVehicles(listVehicle: List<VehicleResponse>) {
        mAdapter.setVehicles(listVehicle)
    }

    override fun loadVehicleDetail(item: VehicleResponse) {

    }

    override fun showLoading(loading: Boolean) {
        pbHome.setVisible(loading)
    }

}
