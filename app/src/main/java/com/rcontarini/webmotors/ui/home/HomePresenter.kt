package com.rcontarini.webmotors.ui.home

import com.rcontarini.webmotors.data.network.model.VehicleResponse

private const val FIRST_PAGE = 0

class HomePresenter : HomeContract.Presenter {

    private lateinit var mView: HomeContract.View
    private var mModel: HomeModel = HomeModel()

    init {
        mModel.attach(this)
    }

    override fun getVehicles(page: Int) {
        mView.showLoading(true)
        val selectedPage = page ?: FIRST_PAGE
        if (selectedPage == FIRST_PAGE) {
        }

        mModel.loadMovies(selectedPage)
    }

    override fun onClickItem(item: VehicleResponse) {
    }

    override fun setVehicles(listVehicle: List<VehicleResponse>) {
        mView.showLoading(false)
        mView.displayVehicles(listVehicle)
    }

    override fun setError(error: Throwable) {
    }

    override fun attach(view: HomeContract.View) {
        mView = view
    }
}