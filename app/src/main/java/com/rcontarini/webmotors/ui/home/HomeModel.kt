package com.rcontarini.webmotors.ui.home

import com.rcontarini.webmotors.data.network.repository.VehicleRepository
import com.rcontarini.webmotors.utils.extensions.singleSubscribe
import io.reactivex.disposables.CompositeDisposable

class HomeModel : HomeContract.Model {

    private var mDisposable = CompositeDisposable()
    private lateinit var mPresenter: HomeContract.Presenter

    override fun loadMovies(page: Int) {
        mDisposable.addAll(VehicleRepository.getVehicle(page).singleSubscribe(
            onSuccess = {
                mPresenter.setVehicles(it)
            },

            onError = {
                mPresenter.setError(it)
            }
        ))

    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
    }

    override fun attach(presenter: HomeContract.Presenter) {
        mPresenter = presenter
    }

}