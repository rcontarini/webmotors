package com.rcontarini.webmotors.ui

class BaseContract {

    interface Presenter<in T> {
        fun attach(view: T)
    }

    interface View {

    }

    interface Model<in T> {
        fun subscribe()
        fun unsubscribe()
        fun attach(presenter: T)
    }
}