package com.rcontarini.webmotors.data.network.repository

import com.rcontarini.webmotors.data.network.datasource.VehicleRemoteDataSource


object VehicleRepository {

    fun getVehicle(page: Int) = VehicleRemoteDataSource.getVehicles(page)

}