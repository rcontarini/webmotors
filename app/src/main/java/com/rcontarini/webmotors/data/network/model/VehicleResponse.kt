package com.rcontarini.webmotors.data.network.model

import java.io.Serializable

data class VehicleResponse(val ID: Int,
                           val Make: String,
                           val Model: String,
                           val Version: String,
                           val Image: String,
                           val KM: Long,
                           val Price: String,
                           val YearModel: Long,
                           val YearFab: Long,
                           val Color: String) : Serializable