package com.rcontarini.webmotors.data.network.exception;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.rcontarini.webmotors.data.network.model.ErrorResponse;

import java.io.IOException;

import br.com.rcontarini.movies.data.network.model.Kind;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RetrofitException extends RuntimeException {

    public static RetrofitException httpError(String url, Response response, Retrofit retrofit){
        Gson gson = new Gson();
        String message = null;
        String errorBody = null;
        try {
            errorBody = response.errorBody().string();
            ErrorResponse error = gson.fromJson(errorBody, ErrorResponse.class);
            if(error.getMessage() != null) {
                message = error.getMessage();
            } else if(error.getMensagem() != null) {
                message = error.getMensagem();
            } else {
                message = response.code() + " " + response.message();
            }
        } catch (IOException | JsonSyntaxException | NullPointerException e) {
            try {
                if (!errorBody.isEmpty()) {
                    message = errorBody;
                } else {
                    message = response.code() + " " + response.message();
                }
            } catch (JsonSyntaxException | NullPointerException error) {
                message = response.code() + " " + response.message();
            }
        }
        return new RetrofitException(message, url, response, Kind.HTTP, null, retrofit);
    }

    public static RetrofitException networkError(IOException exception) {
        return new RetrofitException(exception.getMessage(), null, null, Kind.NETWORK, exception, null);
    }

    public static RetrofitException unexpectedError(Throwable exception) {
        return new RetrofitException(exception.getMessage(), null, null, Kind.UNEXPECTED, exception, null);
    }

    private final String url;
    private final Response response;
    private final Kind kind;
    private final Retrofit retrofit;

    RetrofitException(String message, String url, Response response, Kind kind, Throwable exception, Retrofit retrofit) {
        super(message, exception);
        this.url = url;
        this.response = response;
        this.kind = kind;
        this.retrofit = retrofit;
    }


    public String getUrl() {
        return url;
    }

    public Response getResponse() {
        return response;
    }

    public Kind getKind() {
        return kind;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

}
