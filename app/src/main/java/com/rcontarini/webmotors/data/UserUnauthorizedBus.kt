package com.rcontarini.webmotors.data

import io.reactivex.subjects.PublishSubject

object UserUnauthorizedBus {

    val subject = PublishSubject.create<Any>()

    fun setEvent(error: Any) {
        subject.onNext(error)
    }
}