package com.rcontarini.webmotors.data.network.service

import com.rcontarini.webmotors.data.network.model.VehicleResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface VehicleService {

    @GET("Vehicles?")
    fun getVehicles(@Query("page") page : Int): Single<List<VehicleResponse>>

}