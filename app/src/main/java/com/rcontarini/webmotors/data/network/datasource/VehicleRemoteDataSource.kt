package com.rcontarini.webmotors.data.network.datasource

import com.rcontarini.webmotors.data.ServiceGenerator
import com.rcontarini.webmotors.data.network.interceptors.UnauthorisedInterceptor
import com.rcontarini.webmotors.data.network.service.VehicleService
import com.rcontarini.webmotors.utils.NetworkConstants

object VehicleRemoteDataSource {

    private var mService = ServiceGenerator.createService(
        interceptors = listOf(UnauthorisedInterceptor()),
        serviceClass = VehicleService::class.java,
        url = NetworkConstants.VEHICLES
    )

    fun getVehicles(page: Int) = mService.getVehicles(page = page)
}