package com.rcontarini.webmotors.utils.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat

fun Context.getColorRes(idRes: Int): Int {
    return ContextCompat.getColor(this, idRes)
}

fun Context.getDrawableCompat(@DrawableRes drawableResId: Int) : Drawable? {
    return AppCompatResources.getDrawable(this, drawableResId)
}