package com.rcontarini.webmotors.utils

object NetworkConstants {

    private const val BASE_URL = "http://desafioonline.webmotors.com.br/api/"
    const val VEHICLES = BASE_URL + "OnlineChallenge/"
    const val LANGUAGE = "pt-BR"

    const val CODE_UNKNOWN = 500
}