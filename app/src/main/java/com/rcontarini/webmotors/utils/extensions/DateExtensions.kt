package com.rcontarini.webmotors.utils.extensions

import java.text.SimpleDateFormat
import java.util.*

fun Date.formatToViewDateDefaults(): String{
    val sdf= SimpleDateFormat("yyyy", Locale.getDefault())
    return sdf.format(this)
}



fun String.convertToDate() : Date{
    val sdf= SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    return sdf.parse(this)
}